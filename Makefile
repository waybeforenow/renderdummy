ELM ?= "elm"

elm.js: src/RenderDummy.elm src/DrawingContext.elm
	$(ELM) make $< --output=$@

.PHONY := clean
clean: rm elm.js