port module DrawingContext exposing (..)

import Array exposing (Array, append, empty, foldl, fromList)



-- Uint8 should be 0 - 255


type alias Uint8 =
    Int



-- modulus wrapper for Int -> Uint8


uint8 : Int -> Uint8
uint8 i =
    modBy 256 i


type alias Pixel =
    { r : Uint8
    , g : Uint8
    , b : Uint8
    }


type alias Framebuffer =
    Array Pixel


friendlyArray : Framebuffer -> Array Uint8
friendlyArray image =
    let
        flatten : Pixel -> Array Uint8 -> Array Uint8
        flatten p acc =
            append acc (fromList [ p.r, p.g, p.b, 255 ])
    in
    foldl flatten empty image


port updateCanvas : Array Uint8 -> Cmd msg



-- dumb alert: i have to take at least one argument in this port, which immediately gets discarded painchamp


port getMessage : (Int -> msg) -> Sub msg
