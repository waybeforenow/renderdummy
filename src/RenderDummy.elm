module RenderDummy exposing (..)

import Array exposing (initialize)
import DrawingContext exposing (..)


main : Program () Model Msg
main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    getMessage (\_ -> Msg)


type alias Model =
    { data : Framebuffer
    , ctr : Int
    }


pixelCount : Int
pixelCount =
    320 * 240



-- "Rendering loop"


fakeModel : Int -> Model
fakeModel ctr =
    let
        clampedCtr =
            uint8 ctr
    in
    Model
        (initialize
            pixelCount
            (\n -> Pixel (uint8 clampedCtr + n) (255 - uint8 n + clampedCtr) (uint8 128 + n + clampedCtr))
        )
        ctr


init : () -> ( Model, Cmd Msg )
init _ =
    let
        model =
            fakeModel 0
    in
    ( model, updateCanvas (friendlyArray model.data) )


type alias Msg =
    {}



-- Modify the state to show "animation" potential


update : Msg -> Model -> ( Model, Cmd Msg )
update _ model =
    ( fakeModel (model.ctr + 5), updateCanvas (friendlyArray model.data) )
